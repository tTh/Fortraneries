# Fortraneries

Le Fortran moderne, c'est quand même assez cool, alors autant diffuser
mes gruikeries. Pour l'édification des masses...

J'ai découvert le Fortran moderne lors de mon reclufinement
de Janvier 2022, et j'ai bien aimé. Bon, contrairement à la
version de 77, les `GOTO`s sont moins agréables à faire, mais
l'existence des _pointeurs_ compense largement.

## Le contenu

- [Modules](Modules/) : quelques composants de base.
- [SoundBrotching](SoundBrotching/) : faire gémir vos tympans
- [BloubWorld](BloubWorld/) : la vie des particules
- [Fraktalism](Fraktalism/) : du chaos dans les pixels
- [RandomStuff](RandomStuff/) : on a tous droit à notre jardin secret
- [GrafAnim](GrafAnim/) : Ah, enfin de la gif89a en vue !

## Utilisation

- Prérequis de base, les GNUtrucs : gfortran, gcc, bash, make, awk...
- Première chose à faire, compiler les [modules](Modules/README.md)
  qui seront utilisés par les autres logiciels.
- Et ensuite, à vous de jouer. Fouillez dans les dossiers en sachant
  bien que beaucoup de ces trucs ne sont ni fait, ni à faire.

## Hotline

- Le canal `#tetalab` sur le réseau IRC de
[Libera](https://libera.chat/)
- La [mailing-list publique](https://lists.tetalab.org/mailman/listinfo/tetalab) du Tetalab.

## Ressources

  * [Fortran Programming Language](https://fortran-lang.org/)
  * [Fortran Tips](https://zmoon.github.io/FortranTipBrowser/tips/index.html)

