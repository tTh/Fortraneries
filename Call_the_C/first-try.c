/*
 *		first try of a C func called from Fortran
 */

#include  <stdio.h>
#include  <unistd.h>

/* --------------------------------------------------------------- */
void first_try_(void)
{
fprintf(stderr, "     pid=%u  file='%s'  func='%s' \n",
		(long)getpid(), __FILE__, __func__);
}
/* --------------------------------------------------------------- */
long give_me_my_pid_ (void)
{
pid_t		my_pid;

my_pid = (long)getpid();
fprintf(stderr, " %s -> %d\n", __func__, my_pid);

return my_pid;
}
/* --------------------------------------------------------------- */
