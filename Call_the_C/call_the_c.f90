program call_the_c
  implicit none

  integer                :: foo
  integer, external      :: give_me_my_pid

  print *, "XXX we are calling a C func"
  call first_try ()
  foo = give_me_my_pid()
  print *, "process id = ", foo
  print *, "XXX are we alive ?"

end program


