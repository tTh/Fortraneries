#!/bin/bash

set -e

SRC=WS/data/00013.txt
DST=graph.png

rm WS/graph/*.png

# ----------------------------------------------------------
plot_a_map ()
{
NUMERO=$1

SRC=$(printf "WS/data/%05d.txt"    $NUMERO)
DST=$(printf "WS/graph/%05d.png"   $NUMERO)
TXT=$(printf "mb #%05d"            $NUMERO)

echo "    " $SRC $DST "      " $TXT

gnuplot << __EOC__
set term png    size 512,512
set xrange    [ 0.0 : 1024 ]
set yrange    [ 0.0 : 1024 ]
set output      "${DST}"
set ytics       128
set xtics       128
set grid        front
set title       "${TXT}"
plot            "$SRC"  using 1:2 title "loc"
__EOC__
}
# ----------------------------------------------------------

for foo in $(seq 0 5 1999)
do
	plot_a_map $foo
done

convert -delay 10 WS/graph/0????.png displacement.gif

# ----------------------------------------------------------
