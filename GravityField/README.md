# Gravity Field Experiment

_Some crude experiments to make fancy picture of a useless gravity field._

Expect bug party.

## Le module `realfield`

Les mécaniques sous-jacentes. Sans la moindre rigueur mathématique.

## Le commandeur en chef

C'est le logiciel sobrement nommé `animation` qui n'est absolument
pas fini. Par exemple, il n'est absolument pas paramétrable sans
passer per une recompilation.

## Le raytracing

Vous vous en doutez, c'est du POVray. 

## Sortie graphique

Actuellement, un script pour enrober gnuplot, what else ?

## Conclusion

Enjoy !
