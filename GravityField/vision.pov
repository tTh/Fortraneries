/*
 *	Visualisation en 3d d'une tentative de champ gravitationnel
 *
 *	tTh novembre 2022
 */

#version 3.7;
global_settings {
		ambient_light rgb <0.07, 0.05, 0.05>
		assumed_gamma 1.0
		max_trace_level 6
		}

#include  "colors.inc"

#declare NormClock = clock / 2000.01;
#debug concat("- - - - - - - ", str(NormClock, 7, 5), "\n")

// ----------------------------------------------------------------------

#declare HFDIR  = "WS/nanim/";
#declare HFCK   = mod(clock, 2000);
#declare HFNAME = concat(HFDIR, str(HFCK , -5, 0), ".pgm");

#declare CMDIR  = "WS/colmap/";
#declare CMNAME = concat(CMDIR, str(HFCK , -5, 0), ".pnm");

#debug concat("- - - - - - - ", HFNAME, "  ", CMNAME, "\n")

#declare GravityField = object
{
height_field {
        pgm	HFNAME
        smooth    // on
        translate <-0.5, 0, -0.5>
	}
texture {
	pigment {
		image_map { ppm CMNAME }
		// image_map { png "WS/mire1024.png" }
		rotate x*90
		translate <-0.5, 0, -0.5>
		}
	finish { phong 0.45 }
	}
}

object { GravityField scale <4.10, 0.60, 4.10> }

// ----------------------------------------------------------------------

#if (0)
merge {
	cylinder { <0, -0.5, 0>, <0, 1, 0>, 0.0175  }
	sphere   { <0, 1, 0>, 0.0175  }
	pigment { color Red }
	}
#end

light_source { < -2, 9.3, -7>	color Gray80			}
light_source { < -6, 9.3, -8>	color Orange*0.65		}
light_source { < -15, 2.3, 17>	color Gray70			}

// ----------------------------------------------------------------------

#declare   Xcam = -5.05;
#declare   Ycam =  2.5 - (1.7*NormClock);
#declare   Zcam = -8 + (11.66*NormClock);

camera	{
	// omnimax
	location	<Xcam, Ycam, Zcam>
	look_at		<0, 0, 0>
	right		x*image_width/image_height
	angle		34
	}

// ----------------------------------------------------------------------
