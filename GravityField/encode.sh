#!/bin/bash

if [ $# -ne 2 ] ; then
	echo
	echo "need two arguments:"
	echo "    1) source dir"
	echo "    2) mp4 filename"
	exit 1
fi

SDIR="$1"
FNAME="$2"
echo "Encoding from " $SDIR " to " $FNAME

#
#  trying to guess the format of inoput files
#
firstfile=$(ls -1 $SDIR/* | head -1)
echo "first file :" $firstfile
filetype=$(file $firstfile | awk '{ print $2 }')
echo "file type  :" $filetype

case  $filetype in
	PNG)		extension=".png"		;;
	Netpbm)		extension=".pgm"		;;
	*)		extension=".pnm"		;;
esac
echo "extension  :" $extension

TITLE=$(printf -- '---{ experimental gravity field %d }---' $$)

ffmpeg  -nostdin					\
	-loglevel warning				\
	-y -r 30 -f image2 -i ${SDIR}/%05d${extension}	\
	-metadata artist='---{ tTh }---'		\
	-metadata title="${TITLE}"			\
	-c:v libx264 -pix_fmt yuv420p			\
	$FNAME

echo $FNAME ' ..... [done]'


