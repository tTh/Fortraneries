#!/bin/bash

set -e

POVOPT=" -q9 +a -W1280 -H1024 +WT2 -d -v "
SOURCE="vision.pov"
TMPF="/dev/shm/gravfield.png"

date > pov.stderr

#		---------------------------------------

une_passe ()
{
clock=$1

cp pov.stderr old.stderr

povray -i${SOURCE} -K${clock} $POVOPT -O${TMPF} 2> pov.stderr

timestamp=$(date -u +'%F %H:%M' | tr '01' 'Ol')
texte=$(printf "pass %04d" $clock | tr '01' 'Ol')
outfile=$(printf "WS/troid/%05d.png" $clock)
echo  $timestamp $texte $outfile

convert	${TMPF}				\
	-pointsize 24			\
	-font Courier-Bold		\
	-fill Yellow			\
	-annotate +20+32  "$timestamp"	\
	-annotate +20+58  "$texte"	\
	-pointsize 16			\
	-gravity south-west		\
	-annotate +15+9 "tTh & Konrad"	\
	${outfile}

sleep 6
}

#		---------------------------------------

# 	main loop, build all that nice picz

for foo in $(seq 0 1999)
do
	echo '............' $foo
	une_passe $foo
done

ffmpeg  -nostdin					\
	-loglevel warning				\
	-y -r 30 -f image2 -i WS/troid/%05d.png 	\
	-metadata artist='---{ tTh and Konrad }---'	\
	-metadata title="Experiment on Gravity Field"	\
	-c:v libx264 -pix_fmt yuv420p			\
 	-preset veryslow				\
	gravity-field.mp4

