#!/bin/bash

set -e				# stop on error

make animation

LOG="WS/log.animation"

date >> $LOG

time ./animation | tee -a $LOG

./encode.sh WS/nanim/    gravity-field.mp4
./encode.sh WS/colmap/   gravity-colmap.mp4

ls -rtl *.mp4 >> $LOG
