# What is a bloub ?

## Philosophie

Bonne question, mais nous n'avons pas le temps, point
suivant ? En fait, si, il est tard, mais j'ai envie de
raconter des conneries.

Un bloub est une entité mathématique qui vit
dans un espace cartésien abstrait, bien que normé.
Il peut aussi être borné, soit en mode "boite", soit
en mode "tore". La notion de champ de gravité est
ignoré, parce qu'un bloub se moque d'avoir une masse.
Par contre les bloubs peuvent interagir entre eux
de divers manières : rebond (genre billard), échange
de données, fusion volumique...

## Technique

Un bloub est caractérisé par un certain nombre de valeurs,
rangées dans une structure de donnée.
Ces valeurs représentent des choses comme la position,
la taille, l'age et la couleur d'un bloub.

En voici la version Fortran du _Fri Jan 26 00:58:37 UTC 2024_,
c'est à dire presque (mais pas que) complètement différente
de l'état actuel ou possible du logiciel. Il faut noter que
certains champs ne sont pas encore utilisés.

```
  type t_bloubs
    character(8)         :: nick
    logical              :: alive
    integer              :: state
    integer              :: num               ! ???
    real                 :: px, py, pz
    real                 :: vx, vy, vz
    real                 :: radius
    real                 :: density
    integer              :: age, agemax
    integer              :: red, green, blue
  end type t_bloubs
```

Certains champs sont assez explicites, comme le *nick*,
la position dans l'espace, le rayon (pour nous, un bloub est
une entité abstraite assimilable à une bubulle) ou la vitesse
sur les trois directions de l'espace bloubeux.
D'autres, comme `alive`, sont plus délicates à expliquer,
sauf si l'on considère que les bloubs sont zombifiables.

D'autres, comme age et agemax, sont bien plus sujettes à de diverses
interprétations. doit-on incrémenter l'age à chaque tick d'horloge
ou à chaque évènement discret ? Et à quel age un bloub devient-il
trop vieux, à quel age va-t-il mourir ?

## La fusion des blobs

Quand deux bloubs se rencontrent (en première approche, cela veut
dire que leurs surfaces se recoupent) il y a bien entendu quelque
chose qui se déclenche. En général, c'est la fusion des deux
bloubs. Mais une fusion de bloubs, c'est quoi ?

Je pense qu'il y a une infinité de possibilités, je vais me contenter
d'expliquer la démarche que j'ai suivie.
Tout d'abord, pour la fusion de certains paramètres, comme la position
ou la vitesse, on va faire simple : une moyenne sans pondération.
Précis et efficace.

Pour d'autres (le nick & le num) je n'ai pas d'idée bien précise,
il y a peut-être la notion de « nick dominant » à définir.
Par contre, c'est peut-être sur les valeurs 'corporelles' :
taille, densité, age et couleur qu'il y a des choses à faire.

  * Taille : c'est le `radius` d'une sphere -> somme des volumes
  * Densité : cette valeur n'est actuellement pas gérée
  * Age : (somme des deux ages) * coefficient
  * Agemax : (maximum des deux agemaxs) - forfait
  * Couleurs : un système de mutation selon critères ?

Il ne reste qu'à coder tout ça...

## Analyse de population

Nous avons des moyens assez simple d'enregistrer l'état complet
de la population de bloubs à chaque itération. 
La meilleure preuve étant les vidéos publiés dans les peertubes.
Mais nous devrions plus nous pencher sur les aspects statistiques,
comme la démographie, l'état de santé, la pyramide des ages... 

Les traitements simples peuvent faire appel à Awk et Gnuplot.
Pour les visions plus avancées, un logiciel spécialisé sera
le bienvenu, et **R** est un bon candidat potentiel.
On peut aussi envisager la pureté du code Fortran,
couplé avec un *toolkit* graphique comme XXX.

Il ne reste qu'à coder tout ça...

## Et pour la suite ?

Au fil du temps, le bloub évolue et se complexifie.
La prochaine itération sera dotée d'un attribut de couleur et
d'amusantes fonctions pour mixer ces couleurs si deux bloubs
se trouvent à fusionner.
On peut aussi envisager de les munir d'un spin non entier
dans le but assumé d'augmenter la complexité des rencontres
interbloubs.

<u>tTh, janvier 2024</u>




