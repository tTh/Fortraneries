#!/bin/bash

#
#    how to run this mess in a batch style
#


INCFILE="WS/bloubs.inc"
TMPPNG="/dev/shm/bloubs7.png"
POVOPT="+Q9 +a -v -d -W1024  -H768 -WT2"
DDIR="frames/a"
LOGERR="log.error"
TXTCOLOR="#db4090"

BLOUBDATAS="WS/bloubs.data"
BARYDATAS="WS/log.barycentres"

# --- put the work file in ramdisk
BLBS_IN="/dev/shm/in.blbs"
BLBS_OUT="/dev/shm/out.blbs"

NBIMG=2000

make all
err=$?
if [ $err -ne 0 ] ; then
	echo 'make error code is = ' $err
	exit 1
fi

printf "\n#declare NbImg = %d;\n" $NBIMG > WS/nbimg.inc

#
#    first, we have to make a seminal buch of bloubs
#    --> this function need to be parametrizable
#
./genbloubs ${BLBS_IN}  2

for idx in $(seq 0 $((NBIMG-1)) )
do

	echo "================= run passe $idx ===================="

	# make the bloubs's data readable by POVray
	#
	./exportbloubs ${BLBS_IN} > $BLOUBDATAS
        awk -f toinc.awk < $BLOUBDATAS > $INCFILE
	awk '{ print $1, $2, $3 }' < $BLOUBDATAS > $BARYDATAS

	echo "### raytracing pass $idx"

	povray -Iscene.pov -K${idx} -O${TMPPNG} ${POVOPT} 2> $LOGERR
	if [ 0 -ne $? ] ; then
		tail -15 $LOGERR
		sleep 90
	fi

	td=$(date -u +'%F %R' | tr '01' 'ol')
	hi=$(printf "#%04d" $idx | tr '01' 'ol')
	count=$(tail -1 "WS/log.nb_bloubs" | tr '01' 'ol')

	PNG=$(printf "%s/%05d.png" ${DDIR} $idx)

	convert		${TMPPNG}			\
			-font Courier-Bold		\
			-pointsize 32			\
			-fill "$TXTCOLOR"		\
			-gravity south-east		\
			-annotate +25+5 "$td"		\
			-gravity south-west		\
			-annotate +25+5 "$hi"		\
			-gravity north-east		\
			-annotate +45+5 "$count"	\
			-gravity north-west		\
			-annotate +45+5 "BloubWorld"	\
			$PNG

	echo ' ' $PNG '[done]'

	./movebloubs ${BLBS_IN} ${BLBS_OUT}
	./mergebloubs ${BLBS_OUT} ${BLBS_IN}
	# mv ${BLBS_OUT} ${BLBS_IN}

	echo "### run done"
	sleep 35

done

rm $LOGERR

nice ./encode.sh


