#!/bin/bash

DDIR="frames/a"

ffmpeg  -nostdin					\
        -loglevel warning				\
        -y -r 25 -f image2 -i $DDIR/%05d.png 		\
	-metadata artist='---[ tTh ]---'		\
	-metadata title='---[ BloubWorld alpha ]---'	\
        -c:v libx264 -pix_fmt yuv420p			\
        bloubworld.mp4


