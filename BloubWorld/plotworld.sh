#!/bin/bash

INFILE="out.blbs"
SSV="WS/out.ssv"
IMAGE="dessin.png"

./listbloubs $INFILE > $SSV


timestamp=$(date --utc)

gnuplot << __EOC__
set term png    size 720,720
set output      "${IMAGE}"
set grid        front
set tics	1

set title       "High density bloub world - ${timestamp}"

plot			\
	"${SSV}"  using 5:6 lt rgb "#002090"
__EOC__

echo 'done'
