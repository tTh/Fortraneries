module povstuff

  implicit none

  ! ----------------------------------------------------------------
  type t_boundb
    ! 
    real    :: BBminX, BBmaxX, BBminY, BBmaxY, BBminZ, BBmaxZ
    integer :: id
    ! 
  end type

  contains               ! -----------------------------------------

  subroutine show_bbox( bbox )
    implicit none
    type (t_boundb), intent(in)     :: bbox

    print *, bbox%bbminx, bbox%bbminy, bbox%bbminz
    print *, bbox%bbmaxx, bbox%bbmaxy, bbox%bbmaxz

  end subroutine

  ! ----------------------------------------------------------------

  subroutine start_of_inc_file (fd)
    implicit none
    integer, intent (in)             :: fd

    write(fd, '(A)') "// DON'T EDIT THIS FILE !"

  end subroutine

  ! ----------------------------------------------------------------
  ! we need some primitives for the gestion of colors.
  ! may be a small database indexed by name ?
  !                                XXX
  ! ----------------------------------------------------------------

end module

