program listbloubs

  use bloubspace
  implicit none

  integer, parameter       :: NB_MAX_BLOUBS = 2000000

! --------------------------------------------------------------
  character(300)           :: infile
  integer                  :: errcode, i
  integer                  :: nbgot
  type(t_bloubs), dimension(:), allocatable  :: bloubs

  i = IARGC()
  if (i .ne. 1) then
    STOP ": BAD ARG ON COMMAND LINE"
  endif
  call getarg(1, infile)

  write (0, '(A)') &
            "***** listing bloubs from "//trim(infile)

  allocate (bloubs(NB_MAX_BLOUBS), stat=errcode)
  if (0 .NE. errcode) then
    STOP " : NO ENOUGH MEMORY"
  endif
  ! run a molly-guard
  do i = 1, NB_MAX_BLOUBS
    bloubs(i)%alive = .FALSE.
  enddo

  call slurp_bloubs_file_in_array(trim(infile), bloubs, nbgot)
  write(0, '(A,I6,1X,A)') "slurped ", nbgot, "bloubs"

  do i=1, nbgot
    write(6, '(A8, 1X, 1L, 1X, I2, 1X, F8.3, 1X, 3F8.3, 1X, 3F8.3, 1X, 2I4)')  &
          bloubs(i)%nick, bloubs(i)%alive,               &
          bloubs(i)%state,                               &
          bloubs(i)%radius,                              &
          bloubs(i)%px, bloubs(i)%py, bloubs(i)%pz,      &
          bloubs(i)%vx, bloubs(i)%vy, bloubs(i)%vz,      &
          bloubs(i)%age, bloubs(i)%agemax
  enddo

end program

!                           \o_ 