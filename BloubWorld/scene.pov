/*
 *		BLOUBSWORLD
 *  new Sun 06 Feb 2022 01:33:39 PM CET, rue Ernest Renan
 */
#version 3.7;

global_settings {
                ambient_light rgb <0.14, 0.08, 0.08>
                assumed_gamma 1.0
                }

#include  "colors.inc"

#include  "WS/nbimg.inc"

#declare NormClock = clock / NbImg;

//  ----------------------------------------------------------

#include  "WS/bloubs.inc"

#fopen LC "WS/log.nb_bloubs" append
#write (LC, " ", Nb_Bloubs, "\n")
#fclose LC


object	{
	Bloubs
	finish { phong 0.57 specular 0.57 }
	}

#declare La_Boite = object
{
union {
	plane	{ <1, 0, 0>, -37 	}
	plane	{ <1, 0, 0>,  37	}
	plane	{ <0, 1, 0>, -27	}
	plane	{ <0, 1, 0>,  27	}
	plane	{ <0, 0, 1>,  69 	}
	texture {
		pigment { color srgb <0.225, 0.244, 0.211> } 
		finish { phong 0.18 metallic 0.25 reflection 0.35 }
		}
	}
}

cylinder { 0, -y, 30
	texture {
		pigment { color srgb <0.225, 0.244, 0.211> } 
		finish { phong 0.18 metallic 0.25 reflection 0.10 }
		}
translate -6.20*y
}

sky_sphere {
  pigment { color Gray20 }
  emission rgb <0.01, 0.01, 0.01>
  }

//  ----------------------------------------------------------

#declare Croisillon = object
{
#local SC = 0.75;
union	{
	cylinder { -SC*x, SC*x, 0.04 }
	cylinder { -SC*y, SC*y, 0.04 }
	cylinder { -SC*z, SC*z, 0.04 }
	}
texture	{
	pigment { color Gray50 }
	}
}

object { Croisillon scale 0.90 translate <Bary_X, Bary_Y, Bary_Z> }

//  ----------------------------------------------------------

#declare BH = 6;		// H = taille en horizontal
#declare BV = 6;		// V = taille en vertical
#declare BR = 0.056;

#declare Une_Borne = object
{
merge	{
	cylinder { <0, BV, 0>,   <0, -BV,   0>,   BR     }
	cylinder { <0, 0.042, 0>, <0, -0.042, 0>,   BR*1.90   }
	}
}

#local Ruc = BR * 0.90;
#local Rud = BR * 0.30;

#declare Un_Cadre = object
{
merge {
	cylinder { <-BH, 0, -BH>, <-BH, 0,  BH>,   Ruc    }
	cylinder { < BH, 0, -BH>, < BH, 0,  BH>,   Ruc    }
	cylinder { < BH, 0, -BH>, <-BH, 0, -BH>,   Ruc    }
	cylinder { < BH, 0,  BH>, <-BH, 0,  BH>,   Ruc    }
	cylinder { < BH, 0,   0>, <-BH, 0,   0>,   Rud  }
	cylinder { <  0, 0, -BH>, <  0, 0,  BH>,   Rud  }
	}
texture {
	pigment { color Gray50 }
	finish { phong 0.87 specular 0.57 }
	}
}

#declare Montants = object
{
union	{
	cylinder { <-BH, -BH,   0>, <-BH, BH,   0>,   Rud  }
	cylinder { < BH, -BH,   0>, < BH, BH,   0>,   Rud  }
	cylinder { <  0, -BH, -BH>, <  0, BH, -BH>,   Rud  }
	cylinder { <  0, -BH,  BH>, <  0, BH,  BH>,   Rud  }
	}
texture {
	pigment { color Gray50 }
	finish { phong 0.57 specular 0.87 }
	}
}

#declare Les_Bornes = object
{
union	{
	#local E = 0.0005;
	object { Un_Cadre translate  y*(BV-E)         }
	object { Un_Cadre translate -y*(BV-E)         }

	object { Montants }

	#local G = Green * 0.80;
	object { Une_Borne translate <-BH, 0, -BH> pigment { color Blue*0.9 }  }
	object { Une_Borne translate < BH, 0, -BH> pigment { color G } }
	object { Une_Borne translate <-BH, 0,  BH> pigment { color G } }
	object { Une_Borne translate < BH, 0,  BH> pigment { color Red*0.9 }   }
	}
}

object { Les_Bornes }

//  ----------------------------------------------------------
#local D = 0.016;

#declare Fleche = object
{
merge	{
	sphere   { <-0.05, 0, 0>, D }
	cylinder { <1, 0, 0>, <-0.05, 0, 0>, D }
	cone     { <1, 0, 0>, D*3.1, <1.10, 0, 0>, 0.0001 }
	}
}
#declare Repere = object
{
union {
	object   { Fleche		pigment { color Red }}
	object   { Fleche rotate z*90	pigment { color Green }}
	object   { Fleche rotate y*270	pigment { color Blue }}
	}
}
//  ----------------------------------------------------------

light_source { < 19,  12+NormClock, -17> color Gray80 }
light_source { <-14,  10-NormClock, -29> color Gray70 }

#declare XCAM = 5 - ( 10 * NormClock);
#declare YCAM = -1.1 + (1.25 * NormClock);
#declare ZCAM = -19.20;

#declare XLAT = 0;
#declare YLAT = 0;
#declare ZLAT = 0;

camera  {
	location   <XCAM, YCAM, ZCAM>
	look_at    <XLAT, YLAT, ZLAT>
	right           x*image_width/image_height
	angle		64
	}
