# BloubWorld

C'est quoi ?

Le BloubWorld (que l'on appelle aussi BloubSpace) est un espace borné
dans lequel se déplacent des **bloubs**, lesquels sont
des sortes de particule
munie de certaines propriétés (age, grosseur, vitesses, etc...).
Lesquelles valeurs peuvent évoluer en fonction du temps.
Tout est expliqué dans ce [document](doc/what-is-a-bloub.md).

la structure d'un bloub est (presque) simple, en fait.
Le plus compliqué, c'est de savoir quoi faire de ce fatras
de *bigdata*.
On peut fabriquer des gazillions de bloubs, et ensuite
les lacher dans un espace clôt, avec des parois
rebondissantes.
Chaque choc va un peu les user, et au bout d'un moment,
ils vont mourir. C'est comme ça, c'est la vie des bloubs.

## Comment ça fonctionne ?

Pas trop mal pour un premier jet. Il suffit de lire
le script `runme.sh` pour avoir une idée de l'enchainement
des opérations. Lequel enchainement est décrit plus bas.

Pour le moment, l'ensemble des opérations est gérée par un script shell
qui enchaine des opérations plus élémentaires. Oui, je sais, ce n'est
pas optimal, mais c'est un cadre idéal pour les bricolages hasardeux.

Ces opérations agissent sur des fichiers de type `.blbs` qui sont,
vu du fortran, des dumps séquentiels du type t_bloubs. Un format
de fichier qui va être modifié assez souvent, ne gardez pas d'archives.

## Les logiciels


### genbloubs

Fabrication d'une population de bloubs plus ou moins aléatoires.
Deux paramètres : le nom du fichier (extention `.blbs`)
et le nombre de bloubs désirés.
Les règles de génération *devraient* être paramétrables.
([source](genbloubs.f90))

### movebloubs

Il ne fait que deux choses : à chaque tick, va déplacer
les bloubs et faire naitre de nouveaux bloubs
si le besoin s'en fait sentir.
Seul problème, il n'a pas de notion directe du temps, parce qu'il est juste de passage dans un pipeline.
([source](movebloubs.f90))

### mergebloubs

Le cœur actif du système : c'est lui qui, à chaque tick, va
gérer les rebonds avec la boudary-box, éliminer les
bloubs usés par les chocs, gérer les fusions de bloubs
(avec plein de mathstuff dedans) et assurer l'équilibre
global du système...
C'est sur cette partie qu'il y a des améliorations à trouver.
([source](mergebloubs.f90))

### exportbloubs

Sortie sur `stdout` de certaines propriétes des bloubs, qui seront
reprise par un (ou des) scripts écrits en `awk`, afin de générer
ce qu'il faut pour les différents moteurs de rendu.
**Le format de sortie est susceptible de changer sans préavis.**
Bon, pour le moment, dans les formats il n'y a que POVray,
mais Gnuplot et/ou Rdata arriveront bien un de ces jours.
([source](exportbloubs.f90))

Un exemple : l'idée est de générer un fichier `.inc` pour
Povray pour utiliser les données exportées dans une scène,
par exemple le barycentre des bloubs. Et c'est très facile
à faire avec un [script Awk](toinc.awk).

## TODO

- Concevoir un système de _bouding box_ facile à utiliser
- Réfléchir à une politique de vieillissement des bloubs
- le `merge` de deux bloubs est-il un acte politique ?



