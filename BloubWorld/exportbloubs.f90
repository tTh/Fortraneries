program exportbloubs

  use bloubspace
  implicit none

  character(200)             :: filename
  integer, parameter         :: idu = 33
  integer                    :: i, compte, errcode
  type(t_bloubs)             :: bloub

  !       parsing command line
  i = IARGC()
  if (1 .ne. i) then
    STOP ' : BAD COMMAND LINE'
  endif
  call getarg(1, filename)
  
  write (0, '(A, A)') "### exporting from ", trim(filename)

  open(unit=idu, file=trim(filename), form='unformatted',     &
                 iostat=errcode,                              &
                 action='read', status='old')

  if (0 .ne. errcode) then
    ! print *, 'errcode ', errcode
    STOP " : CAN'T OPEN FILE " // trim(filename)
  endif

  compte=0
  do
    read (unit=idu, iostat=errcode) bloub
    if (0 .ne. errcode) then
      exit
    endif
    if (bloub%alive) then
      print *, bloub%px, bloub%py, bloub%pz, bloub%radius,     &
               bloub%age, bloub%state, "   ",                  &
               bloub%red, bloub%green, bloub%blue
      compte = compte + 1
    endif
  enddo
  
  write(0, '(1X, I0, A)') compte, " bloubs exported"

  close(idu)

end program
