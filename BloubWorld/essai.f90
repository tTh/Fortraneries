program essai

!  use bloubspace
  use mathstuff2
  implicit none

  call test_random(10)

  STOP ': BECAUSE JOB IS DONE'

! --------------------------------------------------------------
         contains

subroutine test_random(nbre)
  implicit none
  integer, intent(in)         :: nbre
  integer                     :: foo
  real                        :: quux, bar
  double precision            :: somme

  call init_random_seed()           ! in module 'mathstuff'
  somme = 0.0
  do foo=1, nbre
    quux = 10.0 * rand()
    somme = somme + quux
    bar = quux ** (.1/.3)
    print *, quux, bar, somme/foo
  enddo
end subroutine test_random
! --------------------------------------------------------------

end program
