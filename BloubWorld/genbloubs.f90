program genbloubs

  use bloubspace
  use mathstuff2

  integer               :: nbbloubs
  integer               :: i
  character(200)        :: filename
  character(30)         :: str
  type(t_bloubs)        :: bloub
  integer               :: idu

  i = IARGC()
  if (i .ne. 2) then
    STOP ": BAD ARGS ON COMMAND LINE"
  endif

  call getarg(1, filename)
  call getarg(2, str)
  read(str,*) nbbloubs

  write (0, '(A,I8,A)') &
  "*** generating ", nbbloubs, " bloubs to "//trim(filename)

  call init_random_seed()

   open(newunit=idu, file=trim(filename),            &
                 form='unformatted',                 &
                 access="sequential",                &
                 action='write', status='replace')

  do i = 1, nbbloubs

    bloub%nick    = 'noname  '
    bloub%num     = i + 41
    call make_a_random_bloub(bloub, 11.80)
    bloub%radius  = 0.010 + (0.12*rand())

    write(idu) bloub              ! no error control ?

  end do

  close(unit=idu)

end program