module mathstuff

  implicit none
  contains

  ! ----------------------------------------------------------------
  !   really quick'n'dirty hack
  !                  not really tested yet...

  subroutine init_random_seed()
    implicit none

    integer, dimension(3) :: tarray
    integer               :: t3, foo
    real                  :: dummy

    call itime(tarray)
    t3 = 3600*tarray(1) + 60*tarray(2) + tarray(3)
    ! write(0, '(A,3I3,A,I6)') " sranding: ", tarray, " --> ", t3
    call srand(t3)

    ! after initializing the random generator engine,
    ! you MUST use it for initializing the initializer
    do foo=1, tarray(1)+15
      dummy = rand()
    enddo

  end subroutine 

  ! ----------------------------------------------------------------
end module mathstuff

