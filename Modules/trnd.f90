program essai

  use mathstuff2
  use pixrgb
  use spitpgm
  use noisepictures
  implicit none

  ! integer                     :: foo, bar

  write(0, *)   "----------------- essai -------------------"

  call init_random_seed()           ! in module 'mathstuff'
  ! call test_noisepictures_rgb()
  call test_noisepictures_rgb_range()
  ! call test_noisepictures_gray()

contains
!-----------------------------------------------------------------------

subroutine test_noisepictures_rgb ()
  implicit none

  type(t_pixrgb), allocatable       :: pix (:,:)
  integer                           :: nombre
  print *, '------ test des noisepictures RGB'

  allocate(pix(800, 600))
  nombre = (800*600)/4

  call rgbpix_set_to_rgb(pix, 0, 0, 0)
  call noise_rgb8_pic(pix, nombre)
  call rgbpix_spit_as_pnm_8(pix, 'foo8.pnm')

  call rgbpix_set_to_rgb(pix, 0, 0, 0)
  call noise_rgb16_pic(pix, nombre)
  call rgbpix_spit_as_pnm_16(pix, 'foo16.pnm')

  deallocate (pix)

end subroutine
!-----------------------------------------------------------------------
! new: Sat Jun 10 06:50:51 UTC 2023

subroutine test_noisepictures_rgb_range ()
  implicit none

  type(t_pixrgb), allocatable       :: pix (:,:)
  integer                           :: nombre
  integer                           :: ranges(6)

  print *, '------ test des noisepictures RGB range'

  allocate(pix(800, 600))
  nombre = (800*600)/4

  call rgbpix_set_to_rgb(pix, 0, 0, 0)
  ranges(1) = 0           ;    ranges(2) = 21000
  ranges(3) = 22000       ;    ranges(4) = 43000
  ranges(5) = 44400       ;    ranges(6) = 63000

  call noise_range_rgb16_pic(pix, ranges, nombre)
  call rgbpix_spit_as_pnm_16(pix, 'rngs16.pnm')

  deallocate (pix)

end subroutine
!-----------------------------------------------------------------------
subroutine test_noisepictures_gray ()
  implicit none

  integer, allocatable       :: pix (:,:)
  integer                           :: nombre
  print *, '------ test des noisepictures GRAY'

  allocate(pix(800, 600))
  nombre = (800*600)/4

  pix = 0
  call noise_gray8_pic(pix, nombre)
  call spit_as_pgm_8(pix, 'bar8.pgm')

  pix = 0
  call noise_gray16_pic(pix, nombre)
  call spit_as_pgm_16(pix, 'bar16.pgm')

  deallocate (pix)

end subroutine

!-----------------------------------------------------------------------

end program
