# General purpose modules


## Modules disponibles

### wavmetrics

This module try to make some computations on *stereo* buffers.

This is just a [WIP](./wavmetrics.f90), see [twavm](./twavm.f90) for a no-use case.

### spitpgm

Write gray level 2d buffer (aka picture) to disk in the NetPNM format.

### pixrgb

Write 8 bits or 16 bits RGB pictures to PNM format.
The width of the picture MUST be a multiple of 4 !

### trials

Experimental WIPs from hell.

### dummy

A "do nothing" useless module.
But you cas use it to fool an optimizing compiler,
or have a sane place to put a breakpoint with gdb

## Compiler un module

*You can use the same options as for a main program.
And when you use the module, you have to specify the paths
for the .mod and the .o to the linker.
*

See [Makefile](./Makefile) for an example.

## TODO

- write the fscking doc !
