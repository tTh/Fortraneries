program t
  use      centermag
  implicit none
  type(t_centermag)      :: cmag

  print *, '====== programme de test centermag ======'
  call essai_centermag(cmag)
  print *

  STOP ': PAF LE CHIEN ?'

!       --------------
contains
!       --------------
subroutine essai_centermag(cm)
    type(t_centermag), intent(inout)   :: cm
    real                               :: rx, ry

    call init_centermag(cm, 800, 600, 1.0)
    call  print_centermag (cm)

    rx = 0.45   ;    ry = -1.098

end subroutine
!       --------------

end program
