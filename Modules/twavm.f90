program twavm

  ! new: Wed Feb  7 01:27:48 UTC 2024

  use mathstuff2
  use wavmetrics
  implicit none

  write(0, *)   "----------------- twavm -------------------"

  call run_second_test(44100/30)
 
contains
!-----------------------------------------------------------------------

subroutine run_first_test(nbs)
  integer, intent(in)   :: nbs                ! nombre d'echantillons

  type(intsample), allocatable     :: samples(:)
  type(wavmetric)                  :: metrics
  integer                          :: foo, bar

  write(0, '(1X, "first test on ", I0, " samples.")') nbs

! create the buffer, and fill it with garbage
  allocate(samples(nbs))
  do foo=1, nbs
    samples(foo)%left   = mod(irand(), 65534) - 32700
    samples(foo)%right  = mod(irand(), 60000) - 29999
  enddo

! compute and display the metrics (gi-go)
  call compute_wavmetric(samples, nbs, metrics)
  call display_wavmetrics(metrics)

end subroutine
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!-
!-  we read the datas from stdin
!-
subroutine run_second_test(nbs)
  integer, intent(in)   :: nbs                ! nombre d'echantillons

  type(intsample), allocatable     :: samples(:)
  type(wavmetric)                  :: metrics
  integer                          :: foo, bar
  integer                          :: vl, vr

  write(0, '(1X, "second test on ", I0, " samples.")') nbs

! create the buffer, and fill it with stdin
  allocate(samples(nbs))
  do foo=1, nbs
    read(5, *) vl, vr
    ! print '(1X, 2I16)', vl, vr
    samples(foo)%left   = vl
    samples(foo)%right  = vr
  enddo

! compute and display the metrics (gi-go)
  call compute_wavmetric(samples, nbs, metrics)
  call display_wavmetrics(metrics)

end subroutine
!-----------------------------------------------------------------------
end program
