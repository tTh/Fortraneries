module mathstuff2

! XXX This module was a copy of mathstuff.f90 fromthe BloubWorld
! XXX                will be moved in an other place some day...

  implicit none
  contains

! ----------------------------------------------------------------
!   really quick'n'dirty hack
!                  not really tested yet...
  subroutine init_random_seed()

    integer, dimension(3) :: tarray
    integer               :: t3, foo
    real                  :: dummy

    call itime(tarray)
    t3 = 3600*tarray(1) + 60*tarray(2) + tarray(3)
    ! write(0, '(A,3I3,A,I6)') "sranding: ", tarray, " --> ", t3
    call srand(t3)

    ! after initializing the random generator engine,
    ! you MUST use it for initializing the initializer
    do foo=1, tarray(1)+15
      dummy = rand()
    enddo

  end subroutine 

! ----------------------------------------------------------------
!-
!-      May be I can make some generic procedures ?
!-
logical function diff_sign(a, b)
   integer, intent(in)     :: a, b

   ! write(0, *) "diff_sign", a, b
   if ( (a .lt. 0) .and. (b .ge. 0) ) then 
      diff_sign = .TRUE.
      return
   endif
   if ( (a .ge. 0) .and. (b .lt. 0) ) then
      diff_sign = .TRUE.
      return
   endif
   diff_sign = .FALSE.

end function
! ----------------------------------------------------------------
end module mathstuff2

