!-------------------------------------------------------------------
!-

program chkpixels

  use   spitpgm              
  use   pixrgb

  use   trials                ! experiments, ymmv.

  implicit none

  write(0, *)  "------ CHKPIXELS ------"
  ! call test_spit_gray(3)
  call test_spit_rgb16(1100, 512)

  STOP 'BECAUSE NO CPU AVAILABLE'

contains
!-------------------------------------------------------------------
!-
!     exerciser for the 'pixrgb' module
!-
  subroutine test_spit_rgb16(sz, kg)
    integer, intent(in)    :: sz, kg

    type(t_pixrgb), allocatable  :: pixrgb(:,:)
    integer                      :: ix, iy

    print *, "test spit rgb", sz
    allocate(pixrgb(sz, sz))
    call rgbpix_set_to_zero(pixrgb)

    do ix=1, sz
      do iy=1, sz
        pixrgb(ix, iy)%r = mod(ix * iy, 65000)
        if (ix.EQ.iy) pixrgb(ix, iy)%g = 65000
        pixrgb(ix, iy)%b = mod ((ix*iy) * 13, 65000)
      end do
    end do
    call rgbpix_spit_as_pnm_16  (pixrgb, "current-rgb16.pnm")
    call new_spit_rgb16         (pixrgb, "experiment-rgb16.pnm")

    deallocate(pixrgb)

  end subroutine
!-------------------------------------------------------------------
!-
  subroutine test_spit_as(increment)
  integer, intent(in)         :: increment

  integer, parameter          :: SZ = 40
  integer, dimension(SZ, SZ)  :: greymap
  integer                     :: ix, iy, value

  print *, "test spit as", sz
  value = 0
  do iy=1, SZ
    do ix=1, SZ
      greymap(ix, iy) = value
      value = value + increment
    enddo
  enddo
!  call spit_as_pgm_16 (greymap, 'a.pnm')
!  call spit_as_pgm_eq (greymap, 'b.pnm')
  call spit_as_pgm_8  (greymap, 'c.pnm')
  call new_spit_a     (greymap, 'x.pnm')
  end subroutine

end program

!-------------------------------------------------------------------
