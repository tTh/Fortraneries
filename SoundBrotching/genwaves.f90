program genwaves

  use soundbrotch
  ! -----------------------------
  implicit none

  integer             :: clock
  ! type(t_sample2i)    :: sample
  real                :: fleft, fright      ! frequencies

  write(0, *) "*** Genwaves ***"
  call soundbrotch_version()

  do clock = 1, 11
    fleft  = real(100 * clock)
    fright = real(116 * clock)
    write(0, '(1X,I8, 9X, F8.2, 6X, F8.2)') clock, fleft, fright
    call sinw_burst2i(6, 23200, fleft, fright, 0.7)
  enddo
  call silence_burst2i(1337)

end program

