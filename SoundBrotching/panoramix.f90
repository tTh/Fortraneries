!-
!		JUST A SMALL EXPERIMENT
!-
program panoramix

  use soundbrotch
! ------------------------------------------------------
  implicit none
  integer                             :: left, right
  integer                             :: errcode
  integer                             :: nblus
  real                                :: value, phi, ka

  write(0, *) "*** Panoramix ***"
  call soundbrotch_version()

  nblus = 0
  do
    read (*, *, iostat=errcode) left, right
    if (errcode .NE. 0) then
      ! write(0, *) 'EOF ? ', errcode
      exit 
    endif

    phi   = real(nblus) / 9500.0
    ka    = 0.500 + (sin(phi) * 0.4999)
    value = (real(left)+real(right)) / 2.05
    left  = int(value * ka)
    right = int(value * (1.0-ka))

    print *, left, right

    nblus = nblus + 1
  enddo

end program

