#!/bin/bash

set -e			# trap on error

DATAFILE="essai.text"
OUTWAV="essai.wav"
make essai

./essai | tee $DATAFILE | c-tools/text2wav $OUTWAV

sndfile-spectrogram				\
	--min-freq=30 --max-freq=2000		\
	--hann					\
	$OUTWAV					\
	1200 600				\
	spectrogram.png
