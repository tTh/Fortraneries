program essai

  use soundbrotch
  ! -----------------------------
  implicit none

  integer          :: n
  real             :: freq

  write(0, *) "*** On essaye des trucs (shotgun!) ***"

  call soundbrotch_version()

  do n = 40, 85
    freq = midi2freq(n)
    write(0, '("   midi", I5, "   -> ", F9.3, " Hz")') n, freq
    call sinw_burst2i(6, 44100, freq, freq, 0.9)
    call silence_burst2i(2900)
  end do

end program
