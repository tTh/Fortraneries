/*
 *	C SUPPORT FUNCTIONS
 */

#include  <stdio.h>
#include  <stdlib.h>

#include   <sndfile.h>

#include  "support.h"

/* --------------------------------------------------------------- */
int display_sf_info(SF_INFO *psf, char *text, int bla)
{

if (bla) {
	fprintf(stderr, "    +-- sf info [%s]    %p\n", text, psf);
	fprintf(stderr, "    |   samplerate          %d\n", psf->samplerate);
	fprintf(stderr, "    |   channels            %d\n", psf->channels);
	fprintf(stderr, "    |   frames              %ld\n", psf->frames);
	fprintf(stderr, "    |   format              0x%x\n", psf->format);
	}
else	{
	fprintf(stderr, "%-25s %6d  %2d %10ld    0x%x\n", text,	\
		psf->samplerate, psf->channels, psf->frames, psf->format);
	}

return 0;
}
/* --------------------------------------------------------------- */
void print_version(char *msg)
{
fprintf(stderr, "=== %s, support compiled %s, %s\n",	\
			msg, __DATE__, __TIME__);
}
/* --------------------------------------------------------------- */
int check_textfile_validity(char *filename, int what)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, filename, what);
#endif

return -1;
}
/* --------------------------------------------------------------- */
