/*
 *		TEXT TO AUDIO OUTPUT
 */

#include  <stdio.h>
#include  <string.h>
#include   <sndfile.h>
#include   <ao/ao.h>		/* for the sound output */

#include  "support.h"

/* --------------------------------------------------------------- */
int	sound_blasting(FILE *input)
{
int			driver;
ao_sample_format	format;
ao_device		* device;

fprintf(stderr, ">>> %s ( %p )\n", __func__, input);

ao_initialize();
driver = ao_default_driver_id();

fprintf(stderr, "    driver is #%d\n", driver);

memset(&format, 0, sizeof(ao_sample_format));	    /* indispensable */

format.bits		= 16;
format.channels		= 2;
format.rate		= 44100;
format.byte_format	= AO_FMT_LITTLE;		/* XXX ??? */
device = ao_open_live(driver, &format, NULL);
if (device == NULL)
	{
	fprintf(stderr, "\n%s: Error open device\n", __func__);
	/*
	 *  comment connaitre la cause de l'erreur ?
	 */
	return -2;
	}
fprintf(stderr, "sound device open at %p\n", device);

return -1;
}
/* --------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int			foo;

print_version(argv[0]);

if (argc < 2) {
	fprintf(stderr, "%s uh?\n", argv[0]);
	}
foo = sound_blasting(stdin);
fprintf(stderr, "sound blasting -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------- */
