/*
 *	TEXT TO WAV       -       a klugeware from tTh 2023
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include   <sndfile.h>

#include  "support.h"

/* --------------------------------------------------------------- */

#define     SMPL_COUNT	8192

/*
 *		WARNING !
 *	this function write only 16bits stereo input
 */
int convert_text_to_wav(FILE *input, char *outf, int sr, int format)
{
SNDFILE			* sndf;
SF_INFO			sfinfo;
short			*buffer;
int			left, right;
int			nb_lus, idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d %d )\n", __func__, outf, sr, format);
#endif

if (format) {
	fprintf(stderr, "in %s, format must be 0, was %d\n",
				__func__,		format);
	exit(1);
	}

/*
 * allocate memory for input garbage
 */
if (NULL==(buffer=malloc(SMPL_COUNT * sizeof(short) * 2))) {
	fprintf(stderr, "%s: MEMORY FULL\n", __func__);
	abort();
	}

memset(&sfinfo, 0, sizeof(sfinfo));		/* be clean */
sfinfo.samplerate    = sr;
sfinfo.channels      = 2;
sfinfo.format        = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

if ( ! (sndf=sf_open(outf, SFM_WRITE, &sfinfo)) ) {
	fprintf(stderr, "write to %s : err %s\n",
				outf, sf_strerror (NULL));
	return -2;
	}

/*
 *  and now, ladies an gentelman, entering the big loop
 */
nb_lus = idx = 0;
memset(buffer, 0, SMPL_COUNT * sizeof(short) * 2);

while (2==fscanf(input, "%d %d", &left, &right)) {
	buffer[idx++] = left;
	buffer[idx++] = right;

	/* SANITY CONTROL */
	if ( (abs(left) > 32767) || (abs(right) > 32767) ) {
		fprintf(stderr, "OVERFLOW sample %9d   values %7d %7d\n",
						idx, left, right);
		}

	if (idx >= SMPL_COUNT) {
		/* flush buffer to file */
		sf_write_short(sndf, buffer, idx);
		idx = 0;
		memset(buffer, 0, SMPL_COUNT * sizeof(short) * 2);
		}

	nb_lus += 1;
	}

sf_write_short(sndf, buffer, idx);
sf_close(sndf);

free(buffer);

fprintf(stderr, "%s: %d samples written\n", __func__, nb_lus);

return 0;
}
/* --------------------------------------------------------------- */
/*
 *	this is a preview software, so no args parsiong yet...
 */
int main(int argc, char *argv[])
{
int			foo;

print_version(argv[0]);

if (2 != argc) {
	fprintf(stderr, "fubarized\n");
	exit(1);
	}

foo = convert_text_to_wav(stdin, argv[1], 44100, 0);
fprintf(stderr, "got a %d from text->wav converter\n", foo);

return 0;
}
/* --------------------------------------------------------------- */
