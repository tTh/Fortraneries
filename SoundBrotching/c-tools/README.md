# C tools

Support utilities for SoundBrotching.

### wav2text

Conversion d'un fichier son en texte machinable, actuellement
en *space separated*, directement utilisable par `Awk`.
Un export `csv` est planifié. Ce programme ne prend
pas (encore) d'options.

Usage : `wav2txt 1337.wav > fichier.txt`

### text2wav

Conversion d'un fichier texte en fichier son.
En principe, réalise l'opération inverse de celle que
fait *wav2text*.

### text2ao

Envoi d'un fichier texte vers une sortie audio.

