/*
 *	C SUPPORT FUNCTIONS
 */

#define		BUFFER_SIZE	8192

/* --------------------------------------------------------- */

int display_sf_info(SF_INFO *psf, char *text, int bla);

void print_version(char *title);

int check_textfile_validity(char *filename, int what);

/* --------------------------------------------------------- */
