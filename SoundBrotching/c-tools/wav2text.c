/*
 *	WAV TO TEXT
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  <sndfile.h>

#include  "support.h"

/* --------------------------------------------------------------- */
/*
 *	at this time, 'format' must be 0
 */
int convert_wav_to_text(char *infname, int format)
{
SNDFILE			* sndf;
SF_INFO			sfinfo;
int			foo, lu;
short			* samples;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, infname);
#endif

if (format) {
	fprintf(stderr, "in %s, format must be 0 and was %d\n",
				__func__,		format);
	exit(1);
	}

memset(&sfinfo, 0, sizeof(sfinfo));		/* be clean */

sndf = sf_open(infname, SFM_READ, &sfinfo);
if (sndf==NULL)
	{
	/*catch the snfile errmsg here XXX */
	fprintf(stderr, "error sf_opening %s\n", infname);
	exit(1);
	}

foo = display_sf_info(&sfinfo, infname, 0);
if (foo) {
	fprintf(stderr, "%s: corrupted sf_info ?\n", __func__);
	abort();
	}
if (2 != sfinfo.channels) {
	fprintf(stderr, "/!\\  %s is not a stereo file, exiting...\n",
				infname);
	return -2;
	}

/* get memory for bufferins read from sound file */
if ( NULL == (samples = malloc(BUFFER_SIZE*sizeof(short))) )
	{
	perror("\n no memory in converter");
	abort();
	}

while ( (lu=sf_read_short(sndf, samples, BUFFER_SIZE)) > 0 ) {
	for (foo=0; foo<lu; foo+=2) {
		printf("%d %d\n", samples[foo], samples[foo+1]);
		}
	}
/*
 *	job done, some cleanup
 */
free(samples);
sf_close(sndf);

return 0;
}
/* --------------------------------------------------------------- */
void usage(void)
{
printf("usage:\n\twav2txt 1337.wav\n");
exit(0);
}
/* --------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int			foo;
int			format = 0;

print_version(argv[0]);

if (2 != argc)		usage();

foo = convert_wav_to_text(argv[1], format);
fprintf(stderr, "got a %d from wav->text converter\n", foo);

return 0;
}
/* --------------------------------------------------------------- */
