#!/bin/bash

INWAV="1637.wav"
OUTWAV="foo.wav"
DATAFILE="waves.text"

# XXX ./wav2text $INWAV | ./panoramix | ./text2wav $OUTWAV

OUTWAV="quux.wav"

./genwaves | tee $DATAFILE | c-tools/text2wav $OUTWAV

sndfile-spectrogram				\
	--min-freq=30 --max-freq=4000		\
	--hann					\
	$OUTWAV					\
	1200 600				\
	spectrogram.png

sndfile-waveform				\
	-c -2					\
	-g 1200x600				\
	$OUTWAV waveform.png

# echo "    HEAD OF DATAS" ; head $DATAFILE
