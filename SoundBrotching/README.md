# SoundBrotching

Stay tuned, film at 440.

## Cheat Code

Certains composants [utiles](c-tools/)
de ce sous-projet fortranique sont
(pour le moment)
ecrits en C, pour un accès facile à des bibliothèques tierces
comme `libsndfile` ou `libao`, composants essentiels du SoundBrotching.

## Not so serious game

Quelques exemples simple, mais bruyants... `runme.sh`

### genwaves

Génération de sons qui arrachent les oreilles.

### panoramix

Conversion de l'entrée en mono, et déplacement en sinusoïdale d'un
coté de l'univers à l'autre.
