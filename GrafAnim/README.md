# GrafAnim

Quelques essais approximatifs pour faire des graphiques inutiles,
dans une démarche mettant en avant la
[techno-futilité](https://wiki.interhacker.space/index.php?title=Techno-futilit%C3%A9),
une notion bien définie par le collectif **Interhack**.

Actuellement, certains des logiciels que vous voyez ici utilisent un backend graphique brassé
[à la maison](https://git.tetalab.org/tTh/libtthimage)
et nommé `genplot2`. Hélas, celui-ci est
un peu foireux sur les tracés de ligne...

## geowaves

Une idée en l'air, probablement...

## trigofest

Distorsions approximatives de la courbe de Lissajous.
Expériences inspirées par [ce site](https://bleuje.com/tutorial1/)
que c'est d'la balle !

## doubledice

Ou comment dessiner des gaussiennes en jetant des dés.

## soundscope

Une tentative de retranscription en image de type oscilloscope/vumètre d'un fichier son.
Les codes source du proggy ([soundscope.f90](soundscope.f90)) et du 
[module](utils_ga.f90) associé sont encore bien *gore*.

Pour convertir le son en données exploitables, il faut utiliser ce [bout de code](../SoundBrotching/c-tools/text2wav.c). Certaines fonctions utilisée par ce logiciel sont dans [utils_ga.f90](utils_ga.f90)
pour la partie dessin.


