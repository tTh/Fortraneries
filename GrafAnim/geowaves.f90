! *******************************************
! 
! *******************************************

program geowaves

  use pixrgb
  implicit none

  integer                       :: width  = 640
  integer                       :: height = 480
  integer                       :: marge  = 10
  type(t_pixrgb), allocatable   :: pix(:,:)
  integer                       :: x, y, h
  real                          :: dist

  allocate(pix(width, height))

  do x=marge, width-marge

    ! write (0, *) "  Y =", y

    do y=marge, height-marge, 5

      print *, x, y
      pix(x, y)%g = 30000

    enddo

  enddo

  call rgbpix_spit_as_pnm_16(pix, "foo.pnm")

end program geowaves

