program doubledice
  use usegenplot
  use utils_ga

  implicit none

  integer                   :: nbarg, numframe
  character(len=256)        :: arg
  integer                   :: idx, foo, bar, xpos
  integer                   :: buckets(12)

  nbarg = IARGC()
  if (nbarg .GT. 0) then
       call GETARG(1, arg)
       ! write (0, '(A40, A5)') "argument = ",  arg
       read (arg, *) numframe
  endif

  write(0, '(" ------------- DOUBLE DICE ----------")')
  write(0, '(" --> frame number:", I5)') numframe
  call init_genplot("dummy.genplot")
  call gplt_rect(0, 0, 800, 600)
  call gplt_setcol(3)
  call gplt_rect(8, 8, 48, 38)
  call gplt_setcol(2)

  buckets = 0
  do idx=1, numframe
    call jouer_un_tour(buckets, idx)
    foo = maxval(buckets)
    ! write(0, *) "max value", foo
  enddo

  call gplt_line(5, 40, 795, 40)
  do bar=1, 12
    xpos = bar*50 + 25
    ! write(0, *) bar, xpos, buckets(bar)
    call gplt_rect(xpos, 41, xpos+50, 41+15*buckets(bar))
  enddo

  call end_genplot("OK boomer...")

contains

! ---------------------------------------------------------

subroutine jouer_un_tour(table, compte)
  integer,intent(inout)   :: table(12)
  integer, intent(in)     :: compte

  integer                 :: DA, DB, valeur

  DA = fair_random_dice()
  DB = fair_random_dice()
  valeur = DA + DB
  ! write(0, *) "pass ", compte, "  =  ", DA, DB, valeur
  table(valeur) = table(valeur) + 1

end subroutine

end program
