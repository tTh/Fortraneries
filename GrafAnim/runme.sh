#!/bin/bash

set -e

SCRATCH="dummy.genplot"

make doubledice

rm -f F/????.tga

for foo in $(seq 1 250)
do
	rm -f dummy.genplot
	./doubledice $foo 
	filename=$(printf "F/%04d.tga" $foo)
	genplot2 -s 640x480 ${SCRATCH} $filename
done

convert -delay 10 F/????.tga foo.gif
