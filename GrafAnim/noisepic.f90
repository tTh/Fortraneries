program noisepic

  use spitpgm
  use pixrgb
  use noisepictures
  use mathstuff2
  implicit none

  integer                   :: numframe = 0

  integer                   :: nbarg
  character(len=256)        :: arg
  integer                   :: ranges(6)
  real                      :: fclock, kpi, r1, r3, r5

  nbarg = IARGC()
  if (nbarg .GT. 0) then
       call GETARG(1, arg)
       ! write (0, '(A40, A5)') "argument = ",  arg
       read (arg, *) numframe
  endif

  call init_random_seed()

  kpi = 3.151592654 / 3.0

  do numframe = 0, 479
    fclock = kpi * float(numframe) / 480.0
    r1 = 27000 + 20000 * cos(fclock*26)
    ranges(1) = nint(r1)    ; ranges(2) = ranges(1)+300

    r3 = 32000 + 28000 * cos(fclock*29)
    ranges(3) = nint(r3)    ; ranges(4) = ranges(3)+300

    r5 = 29000 + 23000 * cos(fclock*32)
    ranges(5) = nint(r5)    ; ranges(6) = ranges(5)+300

    print *, 'r123', numframe, fclock, r1, r3, r5

    call make_noise_color_range_pic (numframe, ranges, 29000)
  enddo

contains
!-- ------------------------------------------------------------------
!--
!-- ------------------------------------------------------------------
subroutine make_noise_color_range_pic (seqv, rngs, nbre)
  implicit none
  integer, intent(in)        :: seqv, nbre
  integer, intent(in)        :: rngs(6)

  type(t_pixrgb), allocatable      :: pix(:,:)
  character (len=280)              :: filename

  allocate(pix(640, 480))
  call rgbpix_set_to_rgb(pix, 0, 0, 0)

  write (filename, "(a, i5.5, a)") "./F/np/", seqv, ".pnm"
  ! print *, 'filename: ', trim(filename)

  call noise_range_rgb16_pic(pix, rngs, nbre)
  call rgbpix_spit_as_pnm_16(pix, trim(filename))

  deallocate(pix)
end subroutine
!-- ------------------------------------------------------------------
end program
