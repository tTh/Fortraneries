program doublegauss
  use pixrgb
  use utils_ga
  implicit none

  type(t_pixrgb), allocatable     ::    pic(:,:)
  character (len=280)             ::    filename
  integer                         ::    pass, iter
  integer                         ::    xrnd, yrnd

  write(0, *) "----- making a doublegauss picture ----"

  allocate(pic(320, 240))
  call rgbpix_set_to_zero(pic)

  do pass=0, 99
    do iter=1, 15000
      xrnd = fair_random_gauss(320)
      yrnd = fair_random_gauss(240)
      ! print *, xrnd, yrnd
      pic(xrnd,yrnd)%r = pic(xrnd,yrnd)%r + 1
      pic(xrnd,yrnd)%g = pic(xrnd,yrnd)%g + 2
      pic(xrnd,yrnd)%b = pic(xrnd,yrnd)%b + 3
    end do

    write (filename, "(a, i5.5, a)") "F/DBG/", pass, ".pnm"
    print *, trim(filename)
    call rgbpix_spit_as_pnm_8 (pic, trim(filename))

  end do

end program

