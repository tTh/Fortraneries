#!/bin/bash

set -e

make noisepic

for foo in $(seq 0 89)
do

	./noisepic $foo

done

convert -delay 10 F/np/*.pnm foo.gif

