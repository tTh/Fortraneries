!-----------------------------------------------------
program essai

  use spitpgm            ! XXX moved in ../Modules 
  use fraktals
  use points3d
  use xperiment

  implicit none

  integer, dimension(:,:), allocatable    ::    picz
  integer                                 ::    W, H, foo
  integer                                 ::    errcode
  character(200)                          ::    filename
  real                                    ::    kx, ky

  write(0, *) "============= essai =============="

  W = 320     ;       H = 240

  call srand(666)

  allocate(picz(W,H), stat=errcode)

  do foo=1, 360
    write (filename, "(a, i5.5, a)") "frames/popcorn/", foo-1, ".pnm"
    write(0, *) "-------->", trim(filename), "<"
    kx   = 50.0 * sin(real(foo)*21.133)
    ky   = 50.0 * cos(real(foo)*26.133)
    write(0, *) foo, kx, ky
    call parasites_0(picz, kx, ky, 233)
    call spit_as_pgm_8(picz, trim(filename))
  enddo

!-----------------------------------------------------
end program
