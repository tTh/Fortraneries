#!/bin/bash

if [ $# -ne 2 ] ; then
	echo
	echo "need two arguments:"
	echo "    source dir"
	echo "    mp4 filename"
	exit 1
fi

SDIR="$1"
FNAME="$2"
echo "Encoding from " $SDIR " to " $FNAME

#
#  trying to guess the format of inoput files
#
firstfile=$(ls -1 $SDIR/* | head -1)
echo "first file :" $firstfile
filetype=$(file $firstfile | awk '{ print $2 }')
echo "file type  :" $filetype

#
# this is BOGUS, replace file by identify ?
#
case  $filetype in
        PNG)            extension=".png"                ;;
        Netpbm)         extension=".pgm"                ;;
        *)              extension=".binary"             ;;
esac
echo "extension  :" $extension



TITLE='---{ experimental }---'

ffmpeg  -nostdin					\
	-loglevel warning				\
	-y -r 30 -f image2 -i $SDIR/%05d.png 		\
	-metadata artist='---{ tTh }---'		\
	-metadata title="${TITLE}"			\
 	-preset veryslow				\
	-c:v libx264 -pix_fmt yuv420p			\
	$FNAME


