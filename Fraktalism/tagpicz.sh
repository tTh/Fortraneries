#!/bin/bash

#
#	THIS IS JUST A QUICK'N'DIRTY HACK !
#	       DO NOT USE IT IN REAL LIFE !
#

set -e

SDIR="frames/spool/"
if [ $# -eq 1 ] ; then
	SDIR="$1"
fi

for img in $SDIR/*.pnm
do

	mogrify						\
		-gravity South-East			\
		-font Courier-Bold			\
		-pointsize 12				\
		-fill Black				\
		-annotate +10+4 "Konrad+tTh 2024"	\
		$img
	echo "tagging " $img

done
