program plotcolmap

  use spitpgm
  use fractcolmap

  implicit none

  integer             :: argc
  character(200)      :: mapname, plotname

! -------check for command line arguments
  argc = IARGC()
  if (2 .NE. argc) then
    STOP 'BECAUSE I NEED TWO ARGS'
  endif
  call getarg(1, mapname)
  call getarg(2, plotname)

  call fcolm_infos('from plotcolmap')
  call fcolm_load_mapfile(trim(mapname))
  call fcolm_plot_mapfile(trim(plotname))

end program