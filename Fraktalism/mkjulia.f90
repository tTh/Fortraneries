!-----------------------------------------------------
!           JULIA
!           =====
!    this is the main program
!-----------------------------------------------------

program julia

    use spitpgm
    use JULIAS
    use PIXRGB

    implicit none

    type(t_pixrgb), allocatable     ::    picz(:,:)
    integer                         ::    argc
    character(200)                  ::    filename, string
    real                            ::    cx, cy

    argc = IARGC()
    if (3 .NE. argc) then
      STOP ": MKJULIA PROGGY NEED 3 PARAMETERS !"
    endif

    call getarg(1, filename)
    call getarg(2, string)       ;   read (string, *) cx
    call getarg(3, string)       ;   read (string, *) cy

    allocate(picz(1280, 1024))

    call julia_colormapped(picz, cx, cy, 0.600, 1000)
    call rgbpix_spit_as_pnm_8(picz, trim(filename))

  contains

!-----------------------------------------------------

end program

!-----------------------------------------------------
