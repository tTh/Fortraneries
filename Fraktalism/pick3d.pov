#version 3.7;

global_settings {
                ambient_light rgb <0.12, 0.04, 0.04>
                assumed_gamma 1.0
                }

#include  "colors.inc"

#declare Tiers = NBPASS * 0.3333333;
#declare CK    = (clock/Tiers)*180;

// --------------------------------------------------------------
#declare Rep = object
{
union {
	#local RB = 0.015;
	cylinder { 0, <2, 0, 0>, RB pigment { color Red   } }
	cylinder { 0, <0, 2, 0>, RB pigment { color Green } }
	cylinder { 0, <0, 0, 2>, RB pigment { color Blue  } }
	}
}
// object { Rep translate <-1, 0.10, -1> }

// --------------------------------------------------------------

#declare TS = 0.025;

#declare Truc = object
{
// box { <-TS, -TS, -TS>, <TS, TS, TS> }
sphere { <0, 0, 0>, TS*0.83 }
}

#include  "WS/pickover.inc"

object {
	object {
		Pickover
		texture {
			pigment { color srgb <0.35, 0.45, 0.80> }
                	finish { phong 0.38 metallic 0.55 }
                	} 
		}

	#if (clock < (Tiers))
	  rotate x*(CK)
	#elseif (clock < (Tiers*2))
	  rotate y*(CK)
	#else 
	  rotate z*(CK)
	#end
	translate y*2.25
	}

//  ----------------------------------------------------------

plane   {
        <0, 1, 0>, 0
        texture {
                pigment { color srgb <0.233, 0.155, 0.191> } 
                finish { phong 0.18 metallic 0.55 }
                }
        }


//  ----------------------------------------------------------

light_source { <-12, 15, -11> color Gray90 }
light_source { < 11, 11,  13> color Gray60 }

#declare XCAM =  -3.1;
#declare YCAM =   3;
#declare ZCAM =   1.9;

camera  {
        location   	<XCAM, YCAM, ZCAM>
        look_at    	<0,    2.09,   0>
        right           x*image_width/image_height
        angle           82
        }
//  ----------------------------------------------------------

