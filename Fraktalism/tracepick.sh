#!/bin/bash

POVOPT=" -d +q9 +a +W1280 +H1024 -v +WT4"
TMPNG="/dev/shm/evolv.png"

outfile=$(printf "frames/pick3d/%05d.png" $1)
# echo $outfile

awk -f pick2pov.awk < WS/pick.dat > WS/pickover.inc

PASS=2222

povray		-ipick3d.pov -K120 ${POVOPT}	\
		Declare=NBPASS=${PASS}		\
		-O${TMPNG} 2> WS/err-tracepick.txt

title="Clifford Pickover strange attractor"
tdate=$(date +'%F %R:%S')
# echo $tdate
coefs=$(tail -1 WS/k-pick.txt)
# echo $coefs
txt=$(printf '%s  %s' "$tdate" "$coefs")

convert		${TMPNG}			\
		-fill  Orange			\
		-font Courier-Bold		\
		-pointsize 32			\
		-gravity North-West		\
		-annotate +30+30 "${title}"	\
		-pointsize 22			\
		-gravity South-West		\
		-annotate +30+30 "${txt}"	\
		$outfile

sleep 2

