! 
!    this is the main programm
! 
!-----------------------------------------------------

program lorentz

    use spitpgm
    use fraktals

    implicit none

    integer, dimension(800, 600)    ::    picz
    integer                         ::    argc
    character(200)                  ::    filename

    argc = IARGC()
    if (1 .NE. argc) then
      STOP ": MKLORENTZ NEED A FILENAME !"
    endif

    call getarg(1, filename)

    write (0, "(A)") "Mk Lorentz -> "//trim(filename)

    call lorentz_0(picz, 50000)
    call spit_as_pgm_8(picz, trim(filename))

end program

!-----------------------------------------------------
