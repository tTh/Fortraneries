module points3d

  implicit none
!-----------------------------------------------------
  type t_point3d
    double precision      :: x, y, z
    integer               :: seq
  end type t_point3d

!-----------------------------------------------------
  contains

!-----------------------------------------------------

subroutine list_points3d(array, start, length)
    type(t_point3d), dimension(:), intent(in)  :: array
    integer, intent(in)                        :: start, length
    integer                                    :: sz, i, j

    write(0, '(1X, A15, 2I9)') "list pt3d ", start, length
    sz = ubound(array, 1)
    if ((start+length) .GT. sz) then
      STOP ' : LIST P3D, OUT OF BOUND'
    endif

    ! send values to stdout.
    do i = 1, length
       j = i + start
       print *, array(j)%x, array(j)%y, array(j)%z, array(j)%seq
    enddo

end subroutine list_points3d

!-----------------------------------------------------

subroutine write_points3d(array, start, length, fname)
    type(t_point3d), dimension(:), intent(in)  :: array
    integer, intent(in)                        :: start, length
    character(*), intent(in)                   :: fname

    integer                                    :: sz, i, j, io

    ! write(0, '(1X, A15, 2I9)') "write pt3d ", start, length
    sz = ubound(array, 1)
    if ((start+length) .GT. sz) then
      STOP ' : WRITE P3D, OUT OF BOUND'
    endif

    open(newunit=io, file=fname)
    do i = 1, length
       j = i + start
       write(io, '(3F12.6, I8)')           &
             array(j)%x, array(j)%y, array(j)%z, array(j)%seq
    enddo
    close(io)

end subroutine write_points3d

!-----------------------------------------------------

end module points3d