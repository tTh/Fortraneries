# Modules


Premier point : trouver les bonnes options de gfortran pour
définir l'emplacement des `.mod`.

Deuxième point : construire un Makefile cohérent d'un bout à l'autre,
avec un script de build bien robuste.

Troisième point : Faire la [documentation](documentation.md)

Quatrilème point : Cultiver la techno-futilité.

