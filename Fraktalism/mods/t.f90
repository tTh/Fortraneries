program t

  use fractcolmap

  call fcolm_infos('from main')
  ! call fcolm_make_gray()

  call fcolm_load_mapfile('volcano.map')
  call fcolm_plot_mapfile('volcano.pnm')

  call fcolm_load_mapfile('neon.map')
  call fcolm_plot_mapfile('neon.pnm')
 
  call fcolm_load_mapfile('chroma.map')
  call fcolm_plot_mapfile('chroma.pnm')
 
  call fcolm_load_mapfile('headache.map')
  call fcolm_plot_mapfile('headache.pnm')
 
  STOP 'BECAUSE YOU WAS OUMPFED.'

contains

!------------------- 

subroutine print_boundaries()
  integer          :: foo
  integer          :: cmp(3)
  do foo=-2, 5
    call fcolm_get_rgb(foo, cmp)
    print *, foo, '     --> ', cmp
  enddo
  do foo=252, 257
    call fcolm_get_rgb(foo, cmp)
    print *, foo, '     --> ', cmp
  enddo
end subroutine

end program

