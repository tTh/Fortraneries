!-----------------------------------------------------
!		MANDELBROT SET
!-
!    refactored Thu 29 Dec 2022 03:21:16 PM CET
!    refactored Sat 31 Dec 2022 12:37:03 PM CET
!        all generative parts are now in 'mandelbrots.f90' module
!-
!-----------------------------------------------------
! 
!    this is the main programm
! 
program mkmandel
    use pixrgb
    use mandelbrots

    implicit none

    type(t_pixrgb), allocatable     ::    pic(:,:)
    integer                         ::    angle
    real                            ::    radangle, radius
    real                            ::    stx, sty
    character (len=80)              ::    filename

    write(0, *) "-------- making some mandelbrot -------"

    allocate(pic(640, 480))

    do angle = 0, 2000

        call rgbpix_set_to_zero(pic)

        radangle = float(angle) * 0.01664
        radius   = float(angle) / 2000.0
        write (filename, "(a, i5.5, a)") "frames/mandel/", angle, ".pnm"
        ! filename = trim(filename)
        write(0,*) "passe ", angle, radangle, trim(filename)

        stx = radius * (sin(radangle*4.07) + 0.2131*sin(radangle*11.36))
        sty = radius * (cos(radangle*6.21) + 0.2725*cos(radangle*10.99))

        call mandelbrot_one(pic, complex(stx, sty))
        call rgbpix_spit_as_pnm_8 (pic, trim(filename))

    enddo

end

!-----------------------------------------------------
