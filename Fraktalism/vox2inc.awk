#!/usr/bin/awk -f

BEGIN	{
	maxcount = 0
	nbrvox = 0
	bx = by = bz = 0.0
	print "// generated file, don't touch it bastard !"
	print "#declare Voxels = object {"
	print "union {"
	}

$4 > 500 {
	count = $4
	value = $5
	if (count > maxcount) 
		{ maxcount = count }
	nbrvox++;
	bx += $1
	by += $2
	bz += $3
	printf "object { VOXEL scale %f translate <%f, %f, %f> }    // %d \n",	\
					value, $1, $2, $3, count
	}

END	{
	print "} } // done, ",     NR, " records read"
	print "#declare VoxMaxcount = ", maxcount, ";"
	print "#declare NbrVox   = ",   nbrvox, ";"
	print "#declare Bary_X   = ", bx/nbrvox, ";"
	print "#declare Bary_Y   = ", by/nbrvox, ";"
	print "#declare Bary_Z   = ", bz/nbrvox, ";"
	}
