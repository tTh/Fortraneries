#!/bin/bash

#
#	build the prog
#
make mkjulia
if [ $? -ne 0 ] ; then
	echo
	echo "Make error " $?
	exit 1
fi

cxa=" -1.5432 "	;	cya=" -0.8999 "
cxb="  1.0975 "	;	cyb="  1.5091 "
nbi=" 2000 "
tmpimg="/dev/shm/juliatmp.pnm"

rm frames/julia/*

#
#	run the prog
#
workdir="frames/julia/"
for foo in $( seq 0 $(( nbi - 1)) )
do
	Ka=$( echo "$foo / $nbi"			| bc -l)
	Kb=$( echo "1.0 - $Ka"				| bc -l)
	# echo $Ka $Kb
	cx=$(echo "($cxa*$Ka) + ($cxb*$Kb)"		| bc -l)
	cy=$(echo "$cya*$Ka + $cyb*$Kb"			| bc -l)

	# make mkjulia

	printf "%5d  %4.6f %4.6f     %4.6f  %4.6f\n"		\
		$foo $Ka $Kb         $cx $cy
	./mkjulia $tmpimg $cx $cy
	echo

	img=$(printf "%s/%05d.png" $workdir $foo)
	tcx=$(printf "%8.6f" $cx)
	tcy=$(printf "%8.6f" $cy)

	convert $tmpimg					\
		-gravity North-East			\
		-font Courier-Bold			\
		-pointsize 20				\
		-fill Yellow				\
		-annotate +15+34 $tcx			\
		-annotate +15+58 $tcy			\
		-gravity South-East			\
		-font Courier				\
		-pointsize 14				\
		-fill Yellow				\
		-annotate +10+6 "Konrad+tTh 2024"	\
		$img

done

echo ; echo "Encoding, please wait..."

./encode.sh frames/julia/ foo.mp4
