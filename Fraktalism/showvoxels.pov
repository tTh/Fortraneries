/*
 *		SHOW VOXELS
 *
 *  see also : vox2inc.awk and voxelize.f90
 */

#version 3.7;

global_settings {
                ambient_light rgb <0.04, 0.04, 0.04>
                assumed_gamma 1.0
                }
//----------------------------------------------------------------

#include  "colors.inc"

#declare VOXEL = object
{
// sphere { 0, 1.18 }
#local D = 2.11;
box { <-D, -D, -D>, <D, D, D> }
}

#include "WS/voxels.inc"
object {
	Voxels
	texture {
		pigment { color White }
		finish { phong 0.6 specular 0.8 }
		}
	/*
	 * un peu de calcul empirique ?
	 */
	#local TRK = DIMC/2.0000000;
	translate <-TRK, -TRK, -TRK>
	// rotate <clock*0.22, 0, clock*0.17>
	}

//----------------------------------------------------------------
#declare TriAxe = object
{
#local Sz = 300;
#local Ra = 0.20;
union	{
	cylinder { <-Sz, 0, 0>, <Sz, 0, 0>, Ra pigment { color Red }    }
	cylinder { <0, -Sz, 0>, <0, Sz, 0>, Ra pigment { color Green }  }
	cylinder { <0, 0, -Sz>, <0, 0, Sz>, Ra pigment { color Blue }   }
	}
finish { phong 0.6 specular 0.8 }
}

object { TriAxe }

plane   {
        <0, 1, 0>, -120
        texture {
                pigment { color srgb <0.203, 0.185, 0.191> } 
                finish { phong 0.18 metallic 0.55 }
                }
        }

light_source { <-29,  45, -27> color Gray70 }
light_source { <-52,  5, -48>   color Yellow*0.45 	}
light_source { < 59,  45,  48>   color Gray20		}
light_source { < 59, -45,  48>   color Gray20		}
light_source { <  9,  59,  18>   color Red*0.65 		}
light_source { <  8,  -48,  3>   color Green*0.75 	}

#include "WS/nbimg.inc"
#declare NormClock = (clock/NbImg);

#declare ECAM    =  210 - (100*NormClock);
#declare CKsmall =  NormClock * 87.20;
#declare Offset  =  0.10;
#declare XCAM  =  ECAM * (sin(radians(CKsmall)) + Offset);
#declare YCAM  =  18;
#declare ZCAM  =  ECAM * (cos(radians(CKsmall)) + Offset);
#declare ACAM  =  65 + (53*NormClock);

// #declare XCAM = ECAM * 0.8;
// #declare ZCAM = ECAM * 0.35;

#if (0 = clock)
  #fopen CL "WS/camvox.log" write
#else 
  #fopen CL "WS/camvox.log" append
#end
#write (CL, clock, " ", NormClock, " ", ECAM, " ", ACAM, "\n")
#fclose CL

camera  {
	location   	<XCAM, YCAM, ZCAM>
	// look_at    	<Bary_X, Bary_Y, Bary_Z>
	look_at		<0, -1, 0>
        right           x*image_width/image_height
        angle           ACAM
        }

//----------------------------------------------------------------
//----------------------------------------------------------------
