! 
!    this is the main programm
! 
!-----------------------------------------------------

program pickover

    use spitpgm
    use points3d 
    use fraktals

    implicit none

    integer, dimension(1024, 768)               ::   picz
    integer                                    ::   argc
    character(200)                             ::   filename
    double precision, dimension(4)             ::   coefs
    type(t_point3d), dimension(:), allocatable ::   points

    integer                                    ::  nbr_points
    integer                                    ::  errcode
    argc = IARGC()
    if (1 .NE. argc) then
      STOP ": PICKOVER NEED A FILENAME !"
    endif

    call getarg(1, filename)
    write (0, "(A)") " *** Pickover -> "//trim(filename)

    nbr_points = 999999
    allocate(points(nbr_points), stat=errcode)
    if (0 .NE. errcode) then
      STOP " : NO ENOUGH MEMORY"
    endif

    coefs(1) =  2.24     ;     coefs(2) =  0.43
    coefs(3) = -0.65     ;     coefs(4) = -2.43

    call compute_pickover(points, coefs)
    call list_points3d(points, 200, 15000)

end program

!-----------------------------------------------------
