#!/bin/bash

POVOPT=" -d +q9 +a +W1280 +H1024 -v +WT2"
PASS=999
ERR="/tmp/pov.error"
POVINC="WS/pickover.inc"
TMPF="/dev/shm/pickover.png"

make pickover
if [ $? -ne 0 ] ; then
	echo
	echo "Make error " $?
	exit 1
fi

./pickover foo.pgm | awk -f pick2pov.awk > $POVINC
# head $POVINC

for pass in $(seq 0 $(( PASS-1 )) )
do

	dstname=$(printf "frames/pick3d/%05d.png" $pass)
	echo $dstname

	povray -ipick3d.pov -K${pass}			\
			Declare=NBPASS=${PASS}		\
			$POVOPT -O${TMPF} 2> $ERR
	if [ $? -ne 0 ]
	then
		tail -20 $ERR
		exit
	fi

	convert		${TMPF}			\
			-fill  Gray50		\
			-gravity South-West	\
			-pointsize 24		\
			-annotate +20+10 "tTh"	\
			$dstname

	sleep  10

done

ffmpeg  -nostdin                                        	\
        -loglevel warning                               	\
        -y -r 25 -f image2 -i frames/pick3d/%05d.png            \
        -metadata artist='---[ tTh ]---'                	\
        -c:v libx264 -pix_fmt yuv420p                   	\
         pick3d.mp4
