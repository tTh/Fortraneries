#!/bin/bash

ASCFILE="nuage.asc"
IMAGE="pickplot.png"

make pickover
if [ $? -ne 0 ] ; then
	echo
	echo "Make error " $?
	exit 1
fi

./pickover foo.pgm > $ASCFILE
if [ $? -ne 0 ] ; then
	echo
	echo "Pickover error " $?
	exit 1
fi


# ----------------------------------------------------
function plot_this_pic()
{
local imgname="$1"
local angle="$2"

printf "== %s == %3d ==\n" $imgname $angle

gnuplot << __EOC__
	set term png    size 1024,768
	set output      "${imgname}"

	set title "3D Pickover"
	unset grid
	unset tics

	set view 70, $angle, 1.20
	set xrange [ -2.10 : 2.10 ]
	set yrange [ -2.10 : 2.10 ]
	set zrange [ -1.00 : 1.00 ]

	splot "${ASCFILE}" notitle with dots lt rgb "blue"
__EOC__
}

# ----------------------------------------------------
ddir="frames"

rm $ddir/p???.png

idx=0
for angle in $(seq 0 5 360)
do

	fname=$(printf "%s/p%03d.png" $ddir $idx)

	plot_this_pic $fname $angle

	idx=$(( idx + 1 ))

done

convert -delay 10 $ddir/p???.png pickover.gif

echo '[done]'

# ------------------------------------------ EOJ -----





