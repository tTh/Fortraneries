#!/bin/bash

echo
source "./common.sh"

make voxelize
if [ $? -ne 0 ] ; then
	echo
	echo "Make error " $?
	exit 1
fi


TMPNG="/dev/shm/voxvidz.png"
NBIMG=1600

printf "#declare NbImg = %d;\n" $NBIMG | tee WS/nbimg.inc

for idx in $( seq   0 $(( NBIMG - 1)) )
do

	dst=$(printf "frames/voxel/%05d.png" $idx)
	delta=$( echo "scale=6 ; ${idx}/${NBIMG}" | bc -l)
	echo "Renderbox work on "$dst"     delta = "  $delta

	./voxelize "WS/voxels.inc" $delta
	grep 'NbrVox' "WS/voxels.inc"

	povray		-ishowvoxels.pov -K$idx ${POVOPT}	\
			-O${TMPNG} 2> WS/toto.err
	if [ $? -ne 0 ] ; then
		echo "ERROR ERROR ERROR ERROR ERROR ERROR"
		tail -15 WS/toto.err
		sleep 20
		#exit 1
	fi

	titre='Voxelisation - tTh - Avril 2022'
	numbers=$(tail -1 WS/camvox.log |			\
		  awk '{printf "  K=%5d  :  %6.3f   %6.3f   %6.3f",	\
					$1, $2, $3, $4}')

	echo "numbers " "$numbers" "     txtidx " $txtidx

	convert		${TMPNG}			\
			-fill  Orange			\
			-kerning 2			\
			-pointsize 32			\
			-font AvantGarde-Book		\
			-gravity South-West		\
			-annotate +20+45 "$titre"	\
			-pointsize 24			\
			-annotate +20+10 "$numbers"	\
							\
			$dst

	grep 'Parse Time' WS/toto.err
	grep 'Trace Time' WS/toto.err

	echo 

done

./encode.sh frames/voxel/ voxel-3.mp4
