# Fraktalism

## Iterative computing inside !

Voyons d'abord
[une vidéo](http://la.buvette.org/fractales/f90/video.html)
qui montre ma première expérience dans ce domaine.

## Trucs à voir

La fractale de Julia se porte plutôt bien, mais les travaux continuent.

* [mkjuliagif.sh](mkjuliagif.sh) : fabrication de la gif animée
* [julias.f90](julias.f90) : fonctions de dessin d'une Julia 
* [mkjulia.f90](mkjulia.f90) : le programme principal

**Q:** pourquoi faire la boucle en shell plutôt qu'en Fortran ?

**A:** Parce que je peux recompiler le binaire `mkjulia` pendant le
déroulement de la boucle, une manière comme une autre de faire
du *livecoding*.

## La technique

Le gros des calculs de fractales est fait dans `mods/fraktals.f90`,
et la gestion des pixels 'physiques' est faite par les
modules externes `spitpgm` et `pixrgb`.

Les fonctions d'usage général sont dans
[mods/](répertoire mods/) ave trop peu
[d'explications](mods/documentation.md)

Des scripts _shell_ sont utilisés pour construire les vidéos.

## File Formats

Certains programmes enregistrent des tables de points 3d dans
des fichiers.

```
  type t_point3d
    double precision      :: x, y, z
    integer               :: seq
  end type t_point3d
```

Generally writen as a *sequencial unformated* file.

## TODO

- Voir de près le calcul du cadrage : [centermag](../Modules/centermag.f90) 
- Rajouter des formules
- Ne pas procastiner sur le reste

## See also

- https://www.maths.town/fractal-articles/mandelbulb/mandelbulb-all-powers/
- https://discuss.pixls.us/t/intriguing-shapes-in-buddhabrot-like-fractals/41816
